//
//  Location+Extras.swift
//  LocationPicker
//
//  Created by Filipe Martins on 09/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import Foundation
import MapKit

extension Location
{
    func e_coordinates() -> CLLocationCoordinate2D {
        guard let _latitude = latitude?.doubleValue else {
            return kCLLocationCoordinate2DInvalid
        }
        guard let _longitude = longitude?.doubleValue else {
            return kCLLocationCoordinate2DInvalid
        }
        return CLLocationCoordinate2DMake(_latitude, _longitude)
    }
    
    func e_name() -> String
    {
        if let _name = self.name , _name != "" { return _name }
        let lat = self.latitude!.doubleValue
        let lon = self.longitude!.doubleValue
        return String(format: "Latitude: %.4f  ,  Longitude: %.4f", arguments: [lat, lon])
    }
}
