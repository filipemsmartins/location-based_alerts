//
//  LocationCell.swift
//  LocationPicker
//
//  Created by Filipe Martins on 08/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import Foundation
import UIKit

class LocationCell : UITableViewCell
{
    static let Identifier: String = "Location.Cell"

    var container    : UIView!
    var action_title : UIButton!
    var icon_arrow   : UIImageView!
    
    var separator_1  : UIView!
    var in_title     : UIButton!
    var lbl_enter_on : UILabel!
    
    var separator_2  : UIView!
    var out_title    : UIButton!
    var lbl_exit_on  : UILabel!
    
    // MARK:- View Initialization
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        loadView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadView()
    {
        container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        container.layer.cornerRadius = 5
        container.backgroundColor = UIColor(red: 39/255, green: 43/255, blue: 51/255, alpha: 1)
        container.clipsToBounds = true
        self.contentView.addSubview(container)
        
        action_title = UIButton(type: UIButtonType.custom)
        action_title.translatesAutoresizingMaskIntoConstraints = false
        action_title.titleLabel!.font = UIFont.e_avenirNextRegular(14)
        action_title.setTitleColor(UIColor.white, for: UIControlState())
        action_title.setTitle("", for: UIControlState())
        action_title.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        action_title.isUserInteractionEnabled = false
        self.contentView.addSubview(action_title)
        
        icon_arrow = UIImageView(image: UIImage(named: "icon_small_arrow_right"))
        icon_arrow.translatesAutoresizingMaskIntoConstraints = false
        icon_arrow.contentMode = UIViewContentMode.center
        self.contentView.addSubview(icon_arrow)
        
        separator_1 = UIView()
        separator_1.translatesAutoresizingMaskIntoConstraints = false
        separator_1.backgroundColor = UIColor(red: 125/255, green: 125/255, blue: 125/255, alpha: 1)
        self.contentView.addSubview(separator_1)
        
        in_title = UIButton(type: UIButtonType.custom)
        in_title.translatesAutoresizingMaskIntoConstraints = false
        in_title.titleLabel!.font = UIFont.e_avenirNextRegular(14)
        in_title.setTitleColor(UIColor(red: 130/255, green: 150/255, blue: 180/255, alpha: 1), for: UIControlState())
        in_title.setTitleColor(UIColor(red: 130/255, green: 150/255, blue: 180/255, alpha: 1), for: UIControlState.selected)
        in_title.setTitle("Alert when Enter location", for: UIControlState())
        in_title.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        in_title.isSelected = true
        in_title.isUserInteractionEnabled = false
        self.contentView.addSubview(in_title)
        
        separator_2 = UIView()
        separator_2.translatesAutoresizingMaskIntoConstraints = false
        separator_2.backgroundColor = UIColor(red: 125/255, green: 125/255, blue: 125/255, alpha: 1)
        self.contentView.addSubview(separator_2)
        
        out_title = UIButton(type: UIButtonType.custom)
        out_title.translatesAutoresizingMaskIntoConstraints = false
        out_title.titleLabel!.font = UIFont.e_avenirNextRegular(14)
        out_title.setTitleColor(UIColor(red: 130/255, green: 150/255, blue: 180/255, alpha: 1), for: UIControlState())
        out_title.setTitleColor(UIColor(red: 130/255, green: 150/255, blue: 180/255, alpha: 1), for: UIControlState.selected)
        out_title.setTitle("Alert when Exit location", for: UIControlState())
        out_title.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        out_title.isSelected = true
        out_title.isUserInteractionEnabled = false
        self.contentView.addSubview(out_title)
        
        lbl_enter_on = UILabel()
        lbl_enter_on.translatesAutoresizingMaskIntoConstraints = false
        lbl_enter_on.font = UIFont.e_avenirNextRegular(14)
        lbl_enter_on.textColor = UIColor(red: 145/255, green: 190/255, blue: 110/255, alpha: 1)
        lbl_enter_on.textAlignment = NSTextAlignment.right
        lbl_enter_on.text = "ON"
        self.contentView.addSubview(lbl_enter_on)
        
        lbl_exit_on = UILabel()
        lbl_exit_on.translatesAutoresizingMaskIntoConstraints = false
        lbl_exit_on.font = UIFont.e_avenirNextRegular(14)
        lbl_exit_on.textColor = UIColor(red: 145/255, green: 190/255, blue: 110/255, alpha: 1)
        lbl_exit_on.textAlignment = NSTextAlignment.right
        lbl_exit_on.text = "ON"
        self.contentView.addSubview(lbl_exit_on)
        
        self.setConstraints()
    }
    
    private func setConstraints()
    {
        self.contentView.removeConstraints(self.constraints)
        
        let views : [String : UIView] = [
            "container"    : container,
            "action_title" : action_title,
            "icon_arrow"   : icon_arrow,
            "separator_1"  : separator_1,
            "in_title"     : in_title,
            "separator_2"  : separator_2,
            "out_title"    : out_title,
            "lbl_enter_on" : lbl_enter_on,
            "lbl_exit_on"  : lbl_exit_on
        ]
        
        self.contentView.e_addVisual("H:|-10-[container]-10-|", inViews: views)
        
        self.contentView.e_addVisual("H:|-20-[action_title]-0-[icon_arrow(==10)]-20-|", inViews: views)
        
        self.contentView.e_addVisual("H:|-10-[separator_1]-10-|", inViews: views)
        
        self.contentView.e_addVisual("H:|-20-[in_title]-0-[lbl_enter_on]-20-|", inViews: views)
        
        self.contentView.e_addVisual("H:|-10-[separator_2]-10-|", inViews: views)
        
        self.contentView.e_addVisual("H:|-20-[out_title]-0-[lbl_exit_on]-20-|", inViews: views)
        
        self.contentView.e_addVisual("V:|-2-[container]-7-|", inViews: views)
        
        self.contentView.e_addVisual("V:|-2-[action_title(==42)]-0-[separator_1(==0.5)]-0-[in_title(==42)]-0-[separator_2(==0.5)]-0-[out_title(==42)]-7-|", inViews: views)
        
        self.contentView.e_addVisual("V:|-2-[icon_arrow(==42)]", inViews: views)
        
        let _ = self.contentView.e_setEqual(lbl_enter_on, relatedTo: in_title,  attribute: .centerY)
        let _ = self.contentView.e_setEqual(lbl_exit_on,  relatedTo: out_title, attribute: .centerY)
    }
}
