//
//  LocationChecker.swift
//  LocationPicker
//
//  Created by Filipe Martins on 08/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import UIKit
import MapKit

class LocationChecker: AsyncChecker, CLLocationManagerDelegate
{
    deinit { self.completation = nil }
    
    lazy var location_manager : CLLocationManager = {
        let location_manager = CLLocationManager()
        location_manager.delegate = self
        return location_manager
    }()
    
    override func checSwitch(complete : @escaping (Bool) -> Void)
    {
        let status = CLLocationManager.authorizationStatus()
        
        if status == CLAuthorizationStatus.notDetermined {
            self.completation = complete
            self.location_manager.requestWhenInUseAuthorization()
            return
        }
        
        DispatchQueue.main.async {
            complete((status == .authorizedAlways) || (status == .authorizedWhenInUse))
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        guard let _completation = completation else { return }
        
        DispatchQueue.main.async {
            _completation((status == .authorizedAlways) || (status == .authorizedWhenInUse))
        }
    }
}
