//
//  NotificationsChecker.swift
//  LocationPicker
//
//  Created by Filipe Martins on 08/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import UIKit
import UserNotifications

class NotificationsChecker: AsyncChecker
{    
    override func checSwitch(complete : @escaping (Bool) -> Void)
    {
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { (granted, error) in
            DispatchQueue.main.async {
                complete(granted)
            }
        })
    }
}
