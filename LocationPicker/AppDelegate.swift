//
//  AppDelegate.swift
//  LocationPicker
//
//  Created by Filipe Martins on 08/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool
    {
        UNUserNotificationCenter.current().delegate = self
        
        let locations = UNNotificationCategory(
            identifier: NotificationsManager.K_CATEGORY_NAME_LOCATION,
            actions: [],
            intentIdentifiers: []
        )
        
        UNUserNotificationCenter.current().setNotificationCategories([locations])
        
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        print("total notifications: \(UIApplication.shared.scheduledLocalNotifications?.count)")
        
        //UIApplication.shared.cancelAllLocalNotifications()
        
        //UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        //UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        
        self.setAppearanceDefaults()
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let controller = MasterViewController()
        let navigator  = NavigationController(rootViewController: controller)
        
        self.window!.rootViewController = navigator
        self.window!.makeKeyAndVisible()
        
        return true
    }
    
    func setAppearanceDefaults()
    {
        var backIconImg = UIImage(named: "icon_go_back")!
        backIconImg = backIconImg.resizableImage(withCapInsets: UIEdgeInsetsMake(1.0, backIconImg.size.width-1.0, backIconImg.size.height-1.0, 1.0))
        UIBarButtonItem.appearance().setBackButtonBackgroundImage(backIconImg, for: UIControlState() , barMetrics: UIBarMetrics.default);
        
        let navbarFont = UIFont.e_avenirNextMedium(20)
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: navbarFont, NSForegroundColorAttributeName:UIColor.white]
        
        UINavigationBar.appearance().setBackgroundImage(UIImage.e_rectWithColor(UIColor(red: 50/255, green: 58/255, blue: 69/255, alpha: 1)), for: UIBarMetrics.default)
        
        UINavigationBar.appearance().shadowImage = UIImage()
        
        UINavigationBar.appearance().tintColor = UIColor.white
    }
}

extension AppDelegate : UNUserNotificationCenterDelegate
{
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        completionHandler()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound])
    }
}

