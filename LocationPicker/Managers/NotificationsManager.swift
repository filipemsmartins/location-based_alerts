//
//  NotificationsManager.swift
//  LocationPicker
//
//  Created by Filipe Martins on 09/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import Foundation
import UserNotifications
import MapKit

class NotificationsManager: NSObject
{
    static let K_CATEGORY_NAME_LOCATION = "com.fmsm.LocationPicker.location"
    static let T_LOCATION_ALERT = "t.location"
    
    static let K_ALERT_TYPE = "k.alert.type"
    static let K_ALERT_UUID = "k.alert.uuid"
    
    static let V_ENTER = "-3NTR3R"
    static let V_EXIT  = "-3X1T"
    
    static func createAlertForLocation(_ location : Location)
    {
        if location.show_alert_on_enter!.boolValue {
            var entryMsg = "You entered in registered location. Don’t forget to check in with your tasks."
            if let _name = location.name , _name.characters.count > 0 {
                entryMsg = String(format:"You entered %@. Don’t forget to check in with your tasks.", _name)
                
            }
            self.createAlertForLocation(location, onEnter : true, onExit : false, message : entryMsg)
        }
        
        if location.show_alert_on_exit!.boolValue {
            var exitMsg = "You exit from registered location. Don’t forget to check in with your tasks."
            if let _name = location.name , _name.characters.count > 0 {
                exitMsg = String(format:"You exit from %@. Don’t forget to check in with your tasks.", _name)
            }
            self.createAlertForLocation(location, onEnter : false, onExit : true, message : exitMsg)
        }
    }
    
    static func removeAlertFromLocation(_ location : Location)
    {
        let region_enter_id = location.uuid! + V_ENTER
        let region_exit_id = location.uuid! + V_EXIT
        let ids = [region_enter_id, region_exit_id]
        
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ids)
        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ids)
    }
    
    static func updateAlertsFromLocation(_ location : Location) {
        self.removeAlertFromLocation(location)
        self.createAlertForLocation(location)
    }
    
    private static func createAlertForLocation(_ location : Location, onEnter enter : Bool, onExit exit : Bool, message : String)
    {
        let regionid = location.uuid! + (enter ? V_ENTER : V_EXIT)
        
        let content = UNMutableNotificationContent()
        content.title = ""
        content.body = message
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = K_CATEGORY_NAME_LOCATION
        content.userInfo = [
            K_ALERT_TYPE : T_LOCATION_ALERT,
            K_ALERT_UUID : location.uuid!
        ]
        
        let region = CLCircularRegion(center: location.e_coordinates(), radius: location.radius!.doubleValue, identifier: regionid)
        region.notifyOnEntry = enter
        region.notifyOnExit  = exit
        
        // Despite the repeat value, the notification doesn't seams to repeat after the first time on a real devise.
        let trigger = UNLocationNotificationTrigger(region: region, repeats: true)
        
        let request = UNNotificationRequest.init(identifier: regionid, content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.add(request)
    }
}
