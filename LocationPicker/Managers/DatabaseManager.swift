//
//  DatabaseManager.swift
//  LocationPicker
//
//  Created by Filipe Martins on 08/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import Foundation
import CoreData
import MapKit

class DatabaseManager : NSObject
{
    static let sharedInstance = DatabaseManager()
    
    private static let DatabaseFileName = "LocationPicker"
    
    fileprivate override init() {
    }
    
    func createLocationWith(coordinate : CLLocationCoordinate2D, radius : Double, alertOnExit exit: Bool, alertOnEnter enter : Bool, name: String?)
    {
        let newLocation = Location(context: self.mainManagedObjectContext)
        newLocation.uuid = UUID().uuidString
        
        self.updateLocation(newLocation, withCoordinate: coordinate, radius: radius, alertOnExit: exit, alertOnEnter: enter, name: name, andNotifications: false)
        
        NotificationsManager.createAlertForLocation(newLocation)
    }
    
    func delete(location : Location)
    {
        NotificationsManager.removeAlertFromLocation(location)
        
        self.mainManagedObjectContext.delete(location)
        
        do {
            try self.mainManagedObjectContext.save()
    
        } catch {
            NotificationsManager.createAlertForLocation(location)
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
    
    func updateLocation(_ location : Location, withCoordinate coordinate : CLLocationCoordinate2D, radius : Double, alertOnExit exit: Bool, alertOnEnter enter : Bool, name: String?)
    {
        self.updateLocation(location, withCoordinate: coordinate, radius: radius, alertOnExit: exit, alertOnEnter: enter, name: name, andNotifications: true)
    }
    
    private func updateLocation(_ location : Location, withCoordinate coordinate : CLLocationCoordinate2D, radius : Double, alertOnExit exit: Bool, alertOnEnter enter : Bool, name: String?, andNotifications : Bool)
    {
        location.latitude = NSNumber(value: coordinate.latitude)
        location.longitude = NSNumber(value: coordinate.longitude)
        location.radius = NSNumber(value: radius)
        location.show_alert_on_enter = NSNumber(value: enter)
        location.show_alert_on_exit = NSNumber(value: exit)
        location.date_create = NSDate()
        location.date_update = NSDate()
        location.name = name
        
        do {
            try self.mainManagedObjectContext.save()
            
            if andNotifications { NotificationsManager.updateAlertsFromLocation(location) }
            
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
    
    // MARK: - Core Data stack
    
    lazy var mainManagedObjectContext: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: DatabaseManager.DatabaseFileName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = self.mainManagedObjectContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
