//
//  AutoView.swift
//  LocationPicker
//
//  Created by Filipe Martins on 08/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import UIKit

class AutoView: UIView
{
    static func addToSuperView(_ superView : UIView) -> AnyObject
    {
        let view = self.init() as UIView
        
        view.translatesAutoresizingMaskIntoConstraints = false
        superView.addSubview(view)
        
        let views : [String : UIView] = ["_view": view]
        
        superView.e_addVisual("V:|-0-[_view]-0-|", inViews: views)
        superView.e_addVisual("H:|-0-[_view]-0-|", inViews: views)
        
        return view
    }
}

