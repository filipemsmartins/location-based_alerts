//
//  LocationPickerView.swift
//  Tasks Time Tracking
//
//  Created by Filipe Martins on 22/10/15.
//  Copyright © 2015 Filipe Martins. All rights reserved.
//

import UIKit
import MapKit

protocol LocationPickerViewDelegate : class
{
    func locationPickerView(_ view : LocationPickerView, shouldZoomToLocation location : CLLocation) -> Bool
    func isEditingLocationForLocationPickerView(_ view : LocationPickerView) -> Bool
    func removeLocationFromLocationPickerView(_ view : LocationPickerView)
}

class LocationPickerView: AutoView
{
    static let btm_height = 60
    
    weak var pdelegate : LocationPickerViewDelegate? = nil
    
    var center_point_width    : NSLayoutConstraint!
    var center_point_height   : NSLayoutConstraint!
    var center_point_center_y : NSLayoutConstraint!
    var center_point          : UIImageView!
    
    var center_shadow_width  : NSLayoutConstraint!
    var center_shadow_height : NSLayoutConstraint!
    var center_shadow        : UIImageView!
    
    var input_container : UIView!
    var input_border    : UIImageView!
    var txf_input       : UITextField!
    
    var map_view : MKMapView!
    
    var radius   : CGFloat = 0
    var circle   : MKCircle?
    
    // ---------------------------------------------
    
    var top_btns_row : UIView!
    var btm_btns_row : UIView!
    
    var btn_current_position : UIButton!
    var btn_remove_location  : UIButton!
    var lbl_radius           : UILabel!
    var lbl_radius_value     : UILabel!
    var radius_slider        : UISlider!
    
    var btn_standard_map    : UIButton!
    var btn_hybrid_map      : UIButton!
    var btn_satellite_map   : UIButton!
    
    var container_contraint : NSLayoutConstraint!
    
    var bottom_s_container : UIView!
    var line_separator     : UIView!
    var lbl_alert_when     : UILabel!
    var lbl_when_enter     : UILabel!
    var switch_enter       : UISwitch!
    var lbl_when_exit      : UILabel!
    var switch_exit        : UISwitch!
    var vertical_separator : UIView!
    var btn_remove         : UIButton!

    // MARK:- View Initializations
    
    func loadView()
    {
        self.backgroundColor = UIColor(red: 50/255, green: 58/255, blue: 69/255, alpha: 1)
        
        map_view = MKMapView()
        map_view.translatesAutoresizingMaskIntoConstraints = false
        map_view.delegate = self
        map_view.showsUserLocation = true
        self.addSubview(map_view)
        

        center_shadow = UIImageView()
        center_shadow.translatesAutoresizingMaskIntoConstraints = false
        center_shadow.image = UIImage(named: "drop_shadow")
        center_shadow.isUserInteractionEnabled = false
        self.addSubview(center_shadow)
        
        
        center_point = UIImageView()
        center_point.translatesAutoresizingMaskIntoConstraints = false
        center_point.image = UIImage(named: "map_pin")
        center_point.isUserInteractionEnabled = false
        self.addSubview(center_point)
        
        /*********************************************************************************************/
        
        center_shadow_width = NSLayoutConstraint(item: center_shadow,
            attribute: NSLayoutAttribute.width,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: NSLayoutAttribute.notAnAttribute,
            multiplier: 1.0, constant: 20)
        
        center_shadow_height = NSLayoutConstraint(item: center_shadow,
            attribute: NSLayoutAttribute.height,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: NSLayoutAttribute.notAnAttribute,
            multiplier: 1.0, constant: 10)
        
        /*********************************************************************************************/
        
        center_point_width = NSLayoutConstraint(item: center_point,
            attribute: NSLayoutAttribute.width,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: NSLayoutAttribute.notAnAttribute,
            multiplier: 1.0, constant: 20)
        
        center_point_height = NSLayoutConstraint(item: center_point,
            attribute: NSLayoutAttribute.height,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: NSLayoutAttribute.notAnAttribute,
            multiplier: 1.0, constant: 60)
        
        center_point_center_y = NSLayoutConstraint(item: center_point,
            attribute: NSLayoutAttribute.centerY,
            relatedBy: NSLayoutRelation.equal,
            toItem: map_view,
            attribute: NSLayoutAttribute.centerY,
            multiplier: 1.0, constant: 0)
        
        /*********************************************************************************************/
        
        if (CLLocationCoordinate2DIsValid(map_view.centerCoordinate)){
            self.addRadiusCircle(map_view.centerCoordinate, radius: 100)
        }
        
        /*********************************************************************************************/
        
        btn_current_position = UIButton(type: UIButtonType.custom)
        btn_current_position.translatesAutoresizingMaskIntoConstraints = false
        btn_current_position.setBackgroundImage(UIImage(named: "btn_dark"), for: UIControlState())
        btn_current_position.setImage(UIImage(named: "icon_user_location"), for: UIControlState())
        btn_current_position.imageView?.contentMode = .center
        btn_current_position.addTarget(self, action: #selector(self.centerOnUserLocation(_:)), for: UIControlEvents.touchUpInside)
        self.addSubview(btn_current_position)
        
        btn_remove_location = UIButton(type: UIButtonType.custom)
        btn_remove_location.translatesAutoresizingMaskIntoConstraints = false
        btn_remove_location.setBackgroundImage(UIImage(named: "btn_remove"), for: UIControlState())
        btn_remove_location.setImage(UIImage(named: "icon_trash"), for: UIControlState())
        btn_remove_location.imageView?.contentMode = .center
        btn_remove_location.addTarget(self, action: #selector(self.removeThisAlert(_:)), for: UIControlEvents.touchUpInside)
        btn_remove_location.isHidden = true
        self.addSubview(btn_remove_location)
        
        /*********************************************************************************************/
        
        top_btns_row = UIView()
        top_btns_row.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(top_btns_row)
        
        lbl_radius = UILabel()
        lbl_radius.translatesAutoresizingMaskIntoConstraints = false
        lbl_radius.font = UIFont.e_avenirNextRegular(14)
        lbl_radius.textColor = UIColor(red: 130/255, green: 150/255, blue: 180/255, alpha: 1)
        lbl_radius.text = "Geofence Radius"
        top_btns_row.addSubview(lbl_radius)
        
        lbl_radius_value = UILabel()
        lbl_radius_value.translatesAutoresizingMaskIntoConstraints = false
        lbl_radius_value.font = UIFont.e_avenirNextRegular(14)
        lbl_radius_value.textColor = UIColor.white
        lbl_radius_value.text = "100 m"
        lbl_radius_value.textAlignment = NSTextAlignment.right
        top_btns_row.addSubview(lbl_radius_value)
     
        radius_slider = UISlider()
        radius_slider.translatesAutoresizingMaskIntoConstraints = false
        radius_slider.minimumValue = 100
        radius_slider.maximumValue = 500
        top_btns_row.addSubview(radius_slider)
        radius_slider.addTarget(self, action: #selector(self.radiusDidChange(_:)), for: UIControlEvents.valueChanged)
        
        /*********************************************************************************************/
        
        input_container = UIView()
        input_container.translatesAutoresizingMaskIntoConstraints = false
        input_container.backgroundColor = UIColor(red: 50/255, green: 58/255, blue: 69/255, alpha: 1)
        self.addSubview(input_container)
        
        input_border = UIImageView()
        input_border.translatesAutoresizingMaskIntoConstraints = false
        input_border.image = UIImage(named: "bkg_input_field")
        input_border.isUserInteractionEnabled = true
        input_container.addSubview(input_border)
        
        txf_input = UITextField()
        txf_input.translatesAutoresizingMaskIntoConstraints = false
        txf_input.font = UIFont.e_avenirNextRegular(14)
        txf_input.textColor = UIColor(red: 39/255, green: 43/255, blue: 51/255, alpha: 1)
        txf_input.textAlignment = NSTextAlignment.left
        txf_input.placeholder = "Set your location a Name"
        txf_input.delegate = self
        input_border.addSubview(txf_input)
        
        /*********************************************************************************************/
        
        bottom_s_container = UIView()
        bottom_s_container.translatesAutoresizingMaskIntoConstraints = false
        bottom_s_container.backgroundColor = UIColor(red: 50/255, green: 58/255, blue: 69/255, alpha: 1)
        self.addSubview(bottom_s_container)
        
        line_separator = UIView()
        line_separator.translatesAutoresizingMaskIntoConstraints = false
        line_separator.backgroundColor = UIColor(red: 125/255, green: 125/255, blue: 125/255, alpha: 1)
        bottom_s_container.addSubview(line_separator)
        
        lbl_alert_when = UILabel()
        lbl_alert_when.translatesAutoresizingMaskIntoConstraints  = false
        lbl_alert_when.text = "Alert when:"
        lbl_alert_when.font = UIFont.e_avenirNextMedium(15)
        lbl_alert_when.textColor = UIColor(red: 130/255, green: 150/255, blue: 180/255, alpha: 1)
        bottom_s_container.addSubview(lbl_alert_when)
        
        switch_enter = UISwitch()
        switch_enter.translatesAutoresizingMaskIntoConstraints = false
        switch_enter.isOn = true
        switch_enter.addTarget(self, action: #selector(self.switchEnter(_:)), for: .valueChanged)
        bottom_s_container.addSubview(switch_enter)
        
        lbl_when_enter = UILabel()
        lbl_when_enter.translatesAutoresizingMaskIntoConstraints = false
        lbl_when_enter.text = "Enter"
        lbl_when_enter.font = UIFont.e_avenirNextRegular(14)
        lbl_when_enter.textColor = UIColor.white
        bottom_s_container.addSubview(lbl_when_enter)
            
        lbl_when_exit = UILabel()
        lbl_when_exit.translatesAutoresizingMaskIntoConstraints = false
        lbl_when_exit.text = "Exit"
        lbl_when_exit.font = UIFont.e_avenirNextRegular(14)
        lbl_when_exit.textColor = UIColor.white
        bottom_s_container.addSubview(lbl_when_exit)
        
        switch_exit = UISwitch()
        switch_exit.translatesAutoresizingMaskIntoConstraints = false
        switch_exit.isOn = true
        switch_exit.addTarget(self, action: #selector(self.switchExit(_:)), for: .valueChanged)
        bottom_s_container.addSubview(switch_exit)
        
        vertical_separator = UIView()
        vertical_separator.translatesAutoresizingMaskIntoConstraints = false
        vertical_separator.backgroundColor = UIColor(red: 125/255, green: 125/255, blue: 125/255, alpha: 1)
        bottom_s_container.addSubview(vertical_separator)
        
        btn_remove = UIButton(type: UIButtonType.custom)
        btn_remove.translatesAutoresizingMaskIntoConstraints = false
        btn_remove.setBackgroundImage(UIImage(named: "btn_remove"), for: UIControlState())
        btn_remove.setTitleColor(UIColor.white, for: UIControlState())
        btn_remove.setTitle("Delete Location", for: UIControlState())
        btn_remove.addTarget(self, action: #selector(self.removeThisAlert(_:)), for: UIControlEvents.touchUpInside)
        btn_remove.titleLabel?.font = UIFont.e_avenirNextMedium(15)
        btn_remove.isHidden = true
        bottom_s_container.addSubview(btn_remove)
        
        container_contraint = NSLayoutConstraint(item: bottom_s_container,
            attribute: NSLayoutAttribute.bottom,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.bottom,
            multiplier: 1.0, constant: 0)
        

        self.showRemoveBtn(self.pdelegate?.isEditingLocationForLocationPickerView(self) ?? false)
        
        self.setConstraints()
    }
    
    private func setConstraints()
    {
        let views : [String : UIView] = [
            "map_view": map_view,
            "input_container": input_container,
            "input_border": input_border,
            "txf_input": txf_input,
            "center_shadow": center_shadow,
            "center_point": center_point,
            "btn_current_position": btn_current_position,
            "btn_remove_location": btn_remove_location,
            "top_btns_row": top_btns_row,
            "bottom_s_container": bottom_s_container,
            "line_separator": line_separator,
            "btn_remove": btn_remove
        ]
        
        let metrics : [String : AnyObject] = ["btm_height" : LocationPickerView.btm_height as AnyObject]
        
        self.e_addVisual("H:|-0-[bottom_s_container]-0-|", inViews: views)
        
        self.addConstraint(self.container_contraint)
        
        // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --|
        
        self.e_addVisual("V:|-0-[input_container(==56)]-0-[map_view]-0-[top_btns_row(==60)]-0-[bottom_s_container(==btm_height)]", inViews: views, withMetrics: metrics)
        
        input_container.e_addVisual("V:|-9-[input_border]-9-|", inViews: views)
        input_border.e_addVisual("V:|-0-[txf_input]-0-|", inViews: views)
        
        self.e_addVisual("H:|-0-[input_container]-0-|", inViews: views)
        input_container.e_addVisual("H:|-10-[input_border]-10-|", inViews: views)
        input_border.e_addVisual("H:|-10-[txf_input]-10-|", inViews: views)
        
        self.e_addVisual("V:[btn_current_position(==40)]-20-[top_btns_row(==60)]-0-[bottom_s_container(==btm_height)]", inViews: views, withMetrics: metrics)
        
        let _ = self.e_setEqual(btn_remove_location, relatedTo: btn_current_position, attribute: .centerY)
        
        self.e_addVisual("V:[btn_remove_location(==40)]", inViews: views)
        
        // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --|
        
        self.e_addVisual("H:|-0-[map_view]-0-|", inViews: views)
        
        self.e_addVisual("H:[btn_remove_location(==40)]-20-[btn_current_position(==40)]-10-|", inViews: views)
        
        // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --|
        
        self.addConstraint(center_shadow_width)
        self.addConstraint(center_shadow_height)
        
        let _ = self.e_setEqual(center_shadow, relatedTo: map_view, attribute: .centerX)
        
        let _ = self.e_setEqual(center_shadow, relatedTo: map_view, attribute: .centerY)
        
        // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --|
        
        self.addConstraint(center_point_width)
        self.addConstraint(center_point_height)
        
        let _ = self.e_setEqual(center_point, relatedTo: map_view, attribute: .centerX)
        
        self.addConstraint(center_point_center_y)
        
        // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --|
        
        self.e_addVisual("H:|-0-[top_btns_row]-0-|", inViews: views)
        
        let top_views : [String : UIView]  = [
            "lbl_radius" : lbl_radius,
            "lbl_radius_value": lbl_radius_value,
            "radius_slider" : radius_slider
        ]
        
        top_btns_row.e_addVisual("H:|-10-[radius_slider]-10-|", inViews: top_views)

        top_btns_row.e_addVisual("H:|-10-[lbl_radius]-0-[lbl_radius_value]-10-|", inViews: top_views)
        
        top_btns_row.e_addVisual("V:|-5-[lbl_radius(==16)]-0-[radius_slider]-6-|", inViews: top_views)
        
        top_btns_row.e_addVisual("V:|-5-[lbl_radius_value(==16)]", inViews: top_views)
        
        // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --|
       
        let remove_views : [String : UIView]  = [
            "line_separator" : line_separator,
            "btn_remove": btn_remove,
            "lbl_alert_when" : lbl_alert_when,
            "lbl_when_enter" : lbl_when_enter,
            "switch_enter"   : switch_enter,
            "lbl_when_exit"  : lbl_when_exit,
            "switch_exit"    : switch_exit,
            "vertical_separator" : vertical_separator
        ]
        
        bottom_s_container.e_addVisual("V:|-0-[line_separator(==0.5)]-0-[lbl_alert_when]-0-[lbl_when_enter]-10-|", inViews: remove_views)
        let _ = bottom_s_container.e_setEqual(lbl_alert_when, relatedTo: lbl_when_enter, attribute: .height)
        let _ = bottom_s_container.e_setEqual(lbl_when_exit, relatedTo: lbl_when_enter, attribute: .height)
        let _ = bottom_s_container.e_setEqual(lbl_when_exit, relatedTo: lbl_when_enter, attribute: .top)
        
        let _ = bottom_s_container.e_setEqual(vertical_separator, relatedTo: lbl_when_enter, attribute: .height)
        let _ = bottom_s_container.e_setEqual(vertical_separator, relatedTo: lbl_when_enter, attribute: .top)
        
        bottom_s_container.e_addVisual("H:|-10-[lbl_alert_when]", inViews: remove_views)
        bottom_s_container.e_addVisual("H:|-10-[lbl_when_enter]-0-[switch_enter]-20-[vertical_separator(==0.5)]-20-[lbl_when_exit]-0-[switch_exit]-10-|", inViews: remove_views)
        let _ = bottom_s_container.e_setEqual(lbl_when_enter, relatedTo: lbl_when_exit, attribute: .width)
        
        let _ = bottom_s_container.e_setEqual(switch_enter, relatedTo: lbl_when_enter, attribute: .centerY)
        let _ = bottom_s_container.e_setEqual(switch_exit, relatedTo: lbl_when_exit, attribute: .centerY)
        
        
        let _ = bottom_s_container.e_setConstant(btn_remove, attribute: .height, constant: 40)
        
        let _ = bottom_s_container.e_setEqual(btn_remove, relatedTo: bottom_s_container, attribute: .centerY)

        bottom_s_container.e_addVisual("H:|-0-[line_separator]-0-|", inViews: remove_views)
        
        bottom_s_container.e_addVisual("H:|-20-[btn_remove]-20-|", inViews: remove_views)
    }
    
    // MARK:- Internal View Methods
    
    func switchEnter(_ sender : AnyObject) {
        if !switch_enter.isOn && !switch_exit.isOn {
            switch_exit.setOn(true, animated: true)
        }
    }
    
    func switchExit(_ sender : AnyObject) {
        if !switch_enter.isOn && !switch_exit.isOn {
            switch_enter.setOn(true, animated: true)
        }
    }
    
    func centerOnUserLocation(_ sender : AnyObject?) {
        guard let location : CLLocation = self.map_view.userLocation.location else { return }
        self.zoomToCoordinate(location.coordinate, animated: true)
    }
    
    func radiusDidChange(_ sender : UISlider) {
        self.lbl_radius_value.text = String(format: "%.0f m", (sender.value))
        self.addRadiusCircle(self.map_view.centerCoordinate, radius: CLLocationDistance(sender.value))
    }
    
    func removeThisAlert(_ sender : AnyObject) {
        self.pdelegate?.removeLocationFromLocationPickerView(self)
    }
    
    // MARK:- External View Methods
    
    func setRadiusValue(_ value : Double)
    {
        self.radius_slider.value = Float(value)
        self.radiusDidChange(self.radius_slider)
        
        self.center_shadow_width.constant = 20
        self.center_shadow_height.constant = 10
        self.center_point_center_y.constant = 0
    }
    
    func currentRadiusValue() -> Double {
        return Double(self.radius_slider.value)
    }
    
    func zoomToCoordinate(_ coordinate : CLLocationCoordinate2D, animated : Bool) {
        let mapRegion = MKCoordinateRegion(center: coordinate, span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
        self.map_view.setRegion(mapRegion, animated: animated)
    }
    
    // MARK:- Helper View Methods
    
    private func showRemoveBtn(_ show : Bool) {
        self.btn_remove_location.isHidden = !show
    }
    
    fileprivate func addRadiusCircle(_ coordinate: CLLocationCoordinate2D, radius:CLLocationDistance)
    {
        guard let c : MKCircle = self.circle else {
            self.circle = MKCircle(center: coordinate, radius: radius as CLLocationDistance)
            self.map_view.add(self.circle!)
            return
        }
        
        self.map_view.remove(c)
        self.circle = nil
        self.addRadiusCircle(coordinate, radius: radius)
    }
}

// MARK:- Map View Delegate Methods

extension LocationPickerView : MKMapViewDelegate
{
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer
    {
        let circle_view = MKCircleRenderer(overlay: overlay)
        circle_view.strokeColor = UIColor.red
        circle_view.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
        circle_view.lineWidth = 1
        return circle_view
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool)
    {
        guard let c : MKCircle = self.circle else { return }
        
        self.map_view.remove(c)
        
        self.center_shadow_width.constant = 40
        self.center_shadow_height.constant = 20
        self.center_point_center_y.constant = -14
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.beginFromCurrentState,
                       animations: {
                        self.center_point.layoutIfNeeded()
                        self.center_shadow.layoutIfNeeded()
            }, completion: nil)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool)
    {
        self.addRadiusCircle(map_view.centerCoordinate, radius: CLLocationDistance(radius_slider.value))
        
        self.center_shadow_width.constant = 20
        self.center_shadow_height.constant = 10
        self.center_point_center_y.constant = 0
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.beginFromCurrentState,
                       animations: {
                        self.center_point.layoutIfNeeded()
                        self.center_shadow.layoutIfNeeded()
            }, completion: nil)
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation)
    {
        guard let location : CLLocation = userLocation.location else { return }
        
        if self.pdelegate?.locationPickerView(self, shouldZoomToLocation: location) ?? true {
            self.zoomToCoordinate(location.coordinate, animated: true)
        }
    }
}

// MARK:- Text Field Delegate Methods

extension LocationPickerView : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
