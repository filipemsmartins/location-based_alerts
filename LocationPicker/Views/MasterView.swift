//
//  MasterView.swift
//  LocationPicker
//
//  Created by Filipe Martins on 08/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import Foundation
import UIKit

protocol MasterViewDelegate : class
{
    func numberOfSections(for masterView: MasterView) -> Int
    func numberOfRows(for masterView: MasterView, in section : Int) -> Int
    func masterView(_ : MasterView, update cell : LocationCell, at index : IndexPath)
    func masterView(_ : MasterView, didSelectRowAt index : IndexPath)
    func masterView(_ : MasterView, removeElementAt index : IndexPath)
}

class MasterView : AutoView
{
    weak var pdelegate : MasterViewDelegate? = nil
    
    var table_view : UITableView!
    
    // MARK:- View Initializations
    
    func loadView()
    {
        self.backgroundColor = UIColor(red: 50/255, green: 58/255, blue: 69/255, alpha: 1)
        
        table_view = UITableView()
        table_view.translatesAutoresizingMaskIntoConstraints = false
        table_view.translatesAutoresizingMaskIntoConstraints = false
        table_view.backgroundColor = UIColor.clear
        table_view.layoutMargins = UIEdgeInsets.zero
        table_view.separatorStyle = UITableViewCellSeparatorStyle.none
        table_view.rowHeight = UITableViewAutomaticDimension
        table_view.estimatedRowHeight = 60
        table_view.contentInset = UIEdgeInsetsMake(10, 0, 10, 0)
        table_view.delegate = self
        table_view.dataSource = self
        self.addSubview(table_view)
        
        self.setConstraints()
    }
    
    private func setConstraints()
    {
        let views : [String : UIView] = [ "table_view"   : table_view ]
        
        self.e_addVisual("V:|-0-[table_view]-0-|", inViews: views)
        self.e_addVisual("H:|-0-[table_view]-0-|", inViews: views)
    }
    
    // MARK:- External View Methods
    
    func endSwipeEditing() {
        self.table_view.setEditing(false, animated: true)
    }
}

// MARK:- Table View Delegate Methods

extension MasterView : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.pdelegate?.numberOfSections(for: self) ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pdelegate?.numberOfRows(for: self, in: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: LocationCell.Identifier) as? LocationCell
        if cell == nil {
            cell = LocationCell(style: UITableViewCellStyle.default, reuseIdentifier: LocationCell.Identifier)
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = UIColor.clear
        }
        self.pdelegate?.masterView(self, update: cell!, at: indexPath)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pdelegate?.masterView(self, didSelectRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let deletetitle = "Delete"
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: deletetitle , handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            self.pdelegate?.masterView(self, removeElementAt: indexPath)
        })
        deleteAction.backgroundColor = UIColor.clear
        
        return [deleteAction]
    }
}
