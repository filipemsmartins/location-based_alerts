//
//  NavigationController.swift
//  LocationPicker
//
//  Created by Filipe Martins on 09/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import Foundation
import UIKit

class NavigationController: UINavigationController
{
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
}
