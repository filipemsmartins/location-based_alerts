//
//  LocationPickerViewController.swift
//  Tasks Time Tracking
//
//  Created by Filipe Martins on 22/10/15.
//  Copyright © 2015 Filipe Martins. All rights reserved.
//

import UIKit
import MapKit

class LocationPickerViewController: UIViewController
{
    var editing_location : Location?
    var locationManager = CLLocationManager()
    
    lazy var main_view: LocationPickerView = {
        let main_view = LocationPickerView.addToSuperView(self.view) as! LocationPickerView
        main_view.pdelegate = self
        return main_view
    }()
    
    // MARK:- Controller Initialization
    
    init(location : Location?) {
        super.init(nibName: nil, bundle: nil)
        self.editing_location = location
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK:- Controller View Cycle
    
    override func loadView() {
        super.loadView()
        self.main_view.loadView()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = (editing_location != nil) ? "Edit Location" : "New Location"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(useLocation(_:)))
        
        self.main_view.txf_input.text = self.editing_location?.name ?? ""
        self.main_view.setRadiusValue(self.editing_location?.radius?.doubleValue ?? 100)
        self.main_view.switch_enter.isOn = self.editing_location?.show_alert_on_enter?.boolValue ?? true
        self.main_view.switch_exit.isOn = self.editing_location?.show_alert_on_exit?.boolValue ?? true
        
        if (!CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied) {
            let msg = "Location services are disabled for this application."
            UIAlertController.e_alertWarningFromVC(self, message: msg)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let coordinate = self.editing_location?.e_coordinates(), CLLocationCoordinate2DIsValid(coordinate) {
            self.main_view.zoomToCoordinate(coordinate, animated: false)
        }
    }
    
    // MARK:- Controller in Actions
    
    func dismissThis() {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func useLocation(_ sender: AnyObject)
    {
        let center : CLLocationCoordinate2D = self.main_view.map_view.centerCoordinate
        if (!CLLocationCoordinate2DIsValid(center)){
            let msg = "Your current location appears to be invalid, please check you location permissions."
            UIAlertController.e_alertErrorFromVC(self, message: msg)
            return
        }
        
        let radius = self.main_view.currentRadiusValue()
        let alertOnExit = self.main_view.switch_exit.isOn
        let alertOnEnter = self.main_view.switch_enter.isOn
        let name = self.main_view.txf_input.text ?? ""
        
        if let _location = editing_location {
            DatabaseManager.sharedInstance.updateLocation(_location, withCoordinate: center, radius: radius, alertOnExit: alertOnExit, alertOnEnter: alertOnEnter, name: name)
        }else {
            DatabaseManager.sharedInstance.createLocationWith(coordinate: center, radius: radius, alertOnExit: alertOnExit, alertOnEnter: alertOnEnter, name: name)
        }
        
        self.dismissThis()
    }
}

// MARK:- Controller View Delegate Methodss

extension LocationPickerViewController : LocationPickerViewDelegate
{
    func locationPickerView(_ view : LocationPickerView, shouldZoomToLocation location : CLLocation) -> Bool {
        let coordinate = self.editing_location?.e_coordinates() ?? kCLLocationCoordinate2DInvalid
        return !CLLocationCoordinate2DIsValid(coordinate)
    }
    
    func isEditingLocationForLocationPickerView(_ view : LocationPickerView) -> Bool {
        return editing_location != nil
    }
    
    func removeLocationFromLocationPickerView(_ view : LocationPickerView)
    {
        guard let _location = editing_location else { return }
        
        let msg = "Do you want to delete selected Location Alert?"
        let _ = UIAlertController.e_askRemoveFromVC(self, removeText: "Delete Location",withMessage: msg) { [weak self] (proceed) -> () in
            if let _self = self {
                if proceed {
                    DispatchQueue.main.async {
                        DatabaseManager.sharedInstance.delete(location: _location)
                        _self.dismissThis()
                    }
                }
            }
        }
    }
}
