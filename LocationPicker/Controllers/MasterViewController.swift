//
//  MasterViewController.swift
//  LocationPicker
//
//  Created by Filipe Martins on 08/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import UIKit
import CoreData

class MasterViewController: UIViewController
{
    var _fetchedResultsController: NSFetchedResultsController<Location>? = nil

    lazy var notification_checker : NotificationsChecker = {
        let notification_checker = NotificationsChecker()
        return notification_checker
    }()
    
    lazy var location_checker : LocationChecker = {
        let location_checker = LocationChecker()
        return location_checker
    }()
    
    lazy var main_view: MasterView = {
        let main_view = MasterView.addToSuperView(self.view) as! MasterView
        main_view.pdelegate = self;
        return main_view
    }()
    
    // MARK:- Controller View Cycle
    
    override func loadView() {
        super.loadView()
        self.main_view.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Locations"
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(pickLocation(_:)))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    // MARK:- Controller in Actions
    
    func pickLocation(_ sender: Any) {
        self.checkNotifications()
    }
    
    private func checkNotifications() {
        self.notification_checker.checSwitch(complete: {[weak self] (proceed : Bool) in
            guard let _self = self else { return }
            if proceed {
                _self.checkLocation()
            }else {
                let msg = "Location based alerts will only work if you allow Notifications on your device settings."
                let _ = UIAlertController.e_alertSettingsFromVC(_self, title: "Warning", message: msg)
            }
        })
    }
    
    private func checkLocation() {
        self.location_checker.checSwitch(complete: { [unowned self] (proceed : Bool) in
            if proceed {
                self.proceedCreateLocation()
            }else {
                let msg = "Location based alerts will only work if you allow Location Services on your device settings."
                UIAlertController.e_alertSettingsFromVC(self, title: "Warning", message: msg)
            }
        })
    }
    
    private func proceedCreateLocation() {
        let controller = LocationPickerViewController(location : nil)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK:- Fetched Results Controller

extension MasterViewController : NSFetchedResultsControllerDelegate
{
    var fetchedResultsController: NSFetchedResultsController<Location>
    {
        if _fetchedResultsController != nil { return _fetchedResultsController! }
        
        let fetchRequest: NSFetchRequest<Location> = Location.fetchRequest()
        fetchRequest.fetchBatchSize = 20
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date_create", ascending: false)]
        
        let context = DatabaseManager.sharedInstance.mainManagedObjectContext
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: "Master")
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.main_view.table_view.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType)
    {
        switch type {
        case .insert:
            self.main_view.table_view.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.main_view.table_view.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?)
    {
        let tableView = self.main_view.table_view
        switch type {
        case .insert:
            tableView?.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView?.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            let cell = tableView?.cellForRow(at: indexPath!) as? LocationCell
            self.configureCell(cell: cell, withLocation: anObject as! Location)
        case .move:
            tableView?.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.main_view.table_view.endUpdates()
    }
}

// MARK:- Controller View Delegate Methodss

extension MasterViewController : MasterViewDelegate
{
    func numberOfSections(for masterView: MasterView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    func numberOfRows(for masterView: MasterView, in section : Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    func masterView(_ : MasterView, update cell : LocationCell, at index : IndexPath) {
        let event = self.fetchedResultsController.object(at: index)
        self.configureCell(cell: cell, withLocation: event)
    }
    
    func configureCell(cell : LocationCell?, withLocation location: Location)
    {
        cell?.action_title.setTitle(location.e_name(), for: UIControlState())
        
        let enter = location.show_alert_on_enter?.boolValue ?? false
        cell?.lbl_enter_on.textColor = enter ? UIColor.e_Green() : UIColor.e_Grey()
        cell?.lbl_enter_on.text = enter ? "ON" : "OFF"
        
        let exit = location.show_alert_on_exit?.boolValue ?? false
        cell?.lbl_exit_on.textColor = exit ? UIColor.e_Green() : UIColor.e_Grey()
        cell?.lbl_exit_on.text = exit ? "ON" : "OFF"
    }
    
    func masterView(_ : MasterView, didSelectRowAt index : IndexPath) {
        let location = self.fetchedResultsController.object(at: index)
        let controller = LocationPickerViewController(location : location)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func masterView(_ : MasterView, removeElementAt index : IndexPath)
    {
        let location = self.fetchedResultsController.object(at: index)
        let msg = "Do you want to delete selected Location?"
        
        let completation : ConfirmCancelAlertBlock = { (proceed) -> () in
            DispatchQueue.main.async {
                if proceed {
                    DatabaseManager.sharedInstance.delete(location: location)
                } else {
                    self.main_view.endSwipeEditing()
                }
            }
        }
        let _ = UIAlertController.e_askRemoveFromVC(self, removeText: "Delete Location", withMessage: msg, completation: completation)
    }
}

