//
//  UIColor+Theme.swift
//  LocationPicker
//
//  Created by Filipe Martins on 09/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import Foundation
import UIKit

extension UIColor
{
    public class func e_Grey() -> UIColor { return UIColor(red: 125/255, green: 125/255, blue: 125/255, alpha: 1) }
    
    public class func e_Green() -> UIColor { return UIColor(red: 145/255, green: 190/255, blue: 110/255, alpha: 1) }
}
