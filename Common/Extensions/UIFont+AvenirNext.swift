//
//  UIFont+AvenirNext.swift
//  LocationPicker
//
//  Created by Filipe Martins on 08/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import UIKit

extension UIFont
{
    public class func e_avenirNextUltraLight(_ size: CGFloat) -> UIFont {
        return self.init(name: "AvenirNext-UltraLight", size:size)!
    }
    
    public class func e_avenirNextRegular(_ size: CGFloat) -> UIFont {
        return self.init(name: "AvenirNext-Regular", size:size)!
    }
    
    public class func e_avenirNextMedium(_ size: CGFloat) -> UIFont {
        return self.init(name: "AvenirNext-Medium", size:size)!
    }
    
    public class func e_avenirNextDemiBold(_ size: CGFloat) -> UIFont {
        return self.init(name: "AvenirNext-DemiBold", size:size)!
    }
}
