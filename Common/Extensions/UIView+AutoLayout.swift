//
//  UIView+AutoLayout.swift
//  LocationPicker
//
//  Created by Filipe Martins on 08/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import Foundation
import UIKit

extension UIView
{    
    func e_addVisual(_ format : String, inViews views : [String : AnyObject])
    {
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format,
            options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views))
    }
    
    func e_addVisual(_ format : String, inViews views : [String : AnyObject], withMetrics metrics : [String : AnyObject])
    {
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format,
            options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
    }
    
    func e_setEqual(_ inView : UIView, relatedTo view : UIView, attribute : NSLayoutAttribute) -> NSLayoutConstraint {
        return e_setEqual(inView, relatedTo: view, attribute: attribute, multiplier: 1.0, constant: 0)
    }
    
    func e_setEqual(_ inView : UIView, relatedTo view : UIView, attribute : NSLayoutAttribute, constant : CGFloat) -> NSLayoutConstraint {
        return e_setEqual(inView, relatedTo: view, attribute: attribute, multiplier: 1.0, constant: constant)
    }
    
    func e_setEqual(_ inView : UIView, relatedTo view : UIView, attribute : NSLayoutAttribute, multiplier : CGFloat) -> NSLayoutConstraint {
        return e_setEqual(inView, relatedTo: view, attribute: attribute, multiplier: multiplier, constant: 0)
    }
    
    func e_setEqual(_ inView : UIView, relatedTo view : UIView, attribute : NSLayoutAttribute, multiplier : CGFloat, constant : CGFloat) -> NSLayoutConstraint
    {
        let constraint = NSLayoutConstraint(item: inView,
            attribute: attribute,
            relatedBy: NSLayoutRelation.equal,
            toItem: view,
            attribute: attribute,
            multiplier: multiplier, constant: constant)
        
        self.addConstraint(constraint)
        
        return constraint
    }
    
    func e_setEqual(_ inView : UIView, attributeA : NSLayoutAttribute, relatedTo toView : UIView, attributeB : NSLayoutAttribute, multiplier : CGFloat, constant : CGFloat) -> NSLayoutConstraint
    {
        let constraint = NSLayoutConstraint(item: inView,
                                            attribute: attributeA,
                                            relatedBy: NSLayoutRelation.equal,
                                            toItem: toView,
                                            attribute: attributeB,
                                            multiplier: multiplier, constant: constant)
        
        self.addConstraint(constraint)
        
        return constraint
    }
    
    func e_setConstant(_ inView : UIView, attribute : NSLayoutAttribute, constant : CGFloat) -> NSLayoutConstraint
    {
        let constraint = NSLayoutConstraint(item: inView,
            attribute: attribute,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1.0, constant: constant)
        
        self.addConstraint(constraint)
        
        return constraint
    }
    
    func e_setSame(_ view : UIView, attribute at1 : NSLayoutAttribute, attribute at2 : NSLayoutAttribute) -> NSLayoutConstraint
    {
        let constraint = NSLayoutConstraint(item: view,
            attribute: at1,
            relatedBy: NSLayoutRelation.equal,
            toItem: view,
            attribute: at2,
            multiplier: 1.0, constant: 0)
        
        self.addConstraint(constraint)
        
        return constraint
    }
}
