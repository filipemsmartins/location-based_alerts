//
//  UIImage+Extras.swift
//  LocationPicker
//
//  Created by Filipe Martins on 09/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import Foundation
import UIKit

extension UIImage
{
    public class func e_rectWithColor(_ color: UIColor) -> UIImage {
        let size = CGSize(width: 2.0, height: 2.0)
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
