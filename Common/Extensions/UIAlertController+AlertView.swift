//
//  UIAlertController+AlertView.swift
//  LocationPicker
//
//  Created by Filipe Martins on 08/10/2016.
//  Copyright © 2016 Filipe Martins. All rights reserved.
//

import UIKit

public typealias ConfirmCancelAlertBlock = ((_ proceed: Bool) -> ())

extension UIAlertController
{
    static func e_askRemoveFromVC(_ vc : UIViewController, removeText : String, withMessage msg : String, completation : @escaping ConfirmCancelAlertBlock) -> UIAlertController
    {
        let alertController = UIAlertController(title: "Warning", message: msg, preferredStyle: .actionSheet)
        
        let remove = UIAlertAction(title: removeText, style: .destructive, handler: { (alert: UIAlertAction!) in
            completation(true)
        })
        alertController.addAction(remove)
        
        let cancel = UIAlertAction(title: "Cancel" , style: .default, handler: { (alert: UIAlertAction!) in
            completation(false)
        })
        alertController.addAction(cancel)
        
        vc.present(alertController, animated: true, completion: nil)
        
        return alertController
    }
    
    static func e_alertErrorFromVC(_ vc: UIViewController, message : String) {
        self.e_alertFromVC(vc, title: "Error", message: message)
    }
    
    static func e_alertWarningFromVC(_ vc: UIViewController, message : String) {
        self.e_alertFromVC(vc, title: "Warning", message: message)
    }
    
    private static func e_alertFromVC(_ vc: UIViewController, title : String, message : String) {
        let alertController = self.e_createAlertController(title : title, message : message)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    static func e_alertSettingsFromVC(_ vc: UIViewController, title : String, message : String) {
        let alertController = self.e_createAlertController(title : title, message : message)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
            if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    private static func e_createAlertController(title : String, message : String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(action)
        return alertController
    }
}
